//
//  BookwormAppApp.swift
//  Shared
//
//  Created by Rizwan on 18/02/2023.
//

import SwiftUI

@main
struct BookwormAppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
